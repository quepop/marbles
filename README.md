# Marbles

A simple marbles game implemented in pure javascript, html and css.

# How to play

To play the game you need to open index.html in your browser, then you can select a marble and move it to an empty slot. Moving the mouse over the board after a marble has been selected draws a red path on it (if it doesn't, the move You are trying to make is illegal) - to confirm a move, you click on an empty slot. You can also deselect a marble by clicking on it again. Setting 5 or more same-color marbles adjacent to each other horizontally, perpendicularly or diagonally results in their disapperance - their count is added to your score. After each move three random marbles are added to the board. The game ends when the board is full and there are no valid moves or there are no marbles left.
