const width = 9;
const height = 9;
const nextColorsSize = 3;
const minGroupSize = 5;
const colors = ["red","blue","yellow","pink","purple","orange","green"];
const precision = 1000000;
const offset = [width,1,-width,-1];
const offsetGroup = [width+1,width,width-1,1];

var fields;
var nextColors;
var path;
var points;
var marbleQuantity;
var selected;
var active;

function init() {
    var board = document.getElementById("board");
    var nextColorsRow = document.getElementById("nextColorsRow");
    points = document.getElementById("points");
    fields = [];
    nextColors = [];
    path = [];
    board.innerHTML = "";
    nextColorsRow.innerHTML = "";
    points.innerHTML = 0;
    marbleQuantity = 1;
    selected = undefined;
    active = false;
    for(var i = 0; i < height; ++i) {
        var row = board.insertRow();
        for(var j = 0; j < width; ++j) fields.push(row.insertCell()), intoField(fields.length-1);
    }
    for(var i = 0; i < nextColorsSize; ++i) {
        nextColors.push(nextColorsRow.insertCell());
        nextColors[i].innerHTML = "<div class=\"marble\"></div>";
    }
    drawRandomColors();
    addMarbles();
    --marbleQuantity;
}

function drawRandomColors() {
    for(var i = 0; i < nextColorsSize; ++i) nextColors[i].firstChild.style.backgroundColor = colors[Math.floor(Math.random()*precision)%colors.length];
}

function addMarbles() {
    if(!marbleQuantity) if(confirm("You won!\nWant to play again?")) {init(); return;}
    for(var i = 0; i < nextColorsSize; ++i) {
        const randomIndex = Math.floor(Math.random()*precision)%(width*height);
        if(fields[randomIndex].innerHTML != "") --i;
        else {
            ++marbleQuantity;
            fields[randomIndex].innerHTML = nextColors[i].innerHTML;
            intoMarble(randomIndex);
            knockOut(randomIndex);
        }
        if(marbleQuantity == width*height) if(confirm("You lost!\nWant to play again?")) {init(); return;}
    }
    drawRandomColors();
}

function shortestPath(start,goal) {  //BFS
    path = [];
    var visited = {};
    var queue = [start];
    while(queue.length) {
        var current = queue.shift();
        if(current == goal) break;
        for(var i = 0; i < offset.length; ++i) {
            var indexOffset = current+offset[i];
            if(fields[indexOffset] == undefined || visited[indexOffset] != undefined || fields[indexOffset].innerHTML != ""  ||  Math.abs(indexOffset%width - current%width) > 1) continue;
            queue.push(indexOffset);
            visited[indexOffset] = current; 
        }
    }
    for(var index = goal; index != start && visited[index] != undefined; index = visited[index]) path.push(visited[index]);
    if(path.length) path.unshift(goal);
}

function marbleClicked(index) {
    var bordering = 4;
    for(var i = 0; i < offset.length; ++i) {
        var indexOffset = index+offset[i]; 
        if(fields[indexOffset] == undefined || fields[indexOffset].innerHTML != "" || Math.abs(indexOffset%width - index%width) > 1) --bordering;
    }
    if(active || !bordering) return;
    if(selected != index) { 
        if(selected != undefined) fields[selected].firstChild.removeAttribute("id");
        fields[index].firstChild.id = "selected", selected = index;
    }
    else fields[index].firstChild.removeAttribute("id"), selected = undefined;
}

function fillPath(color) {
    for(var i = 0; i < path.length; ++i) fields[path[i]].style.backgroundColor = color;
}

function fieldMouseover(index) {
    if(selected == undefined || active) return;
    fillPath("white");
    shortestPath(selected,index);
    fillPath("salmon");
}

function fieldClicked(index) {
    if(selected == undefined || active) return;
    var source = selected;
    marbleClicked(selected);
    if(!path.length) return;
    active = true;
    var iteration = 1
    for(var i = path.length-2; i >= 0; --i) {
        var direction = Math.abs(path[i]-path[i+1]) == 1 ? "left" : "top";
        var multiplier = (path[i]-path[i+1]) < 0 ? -1.0746666666 : 1.0746666666;
        var updatePosition = function(a,b){fields[source].firstChild.style[a] = parseFloat(fields[source].firstChild.style[a]) + b + "px"}
        for(var j = 1; j < 51; ++j) setTimeout(updatePosition,2*iteration++,direction,multiplier)
    }
    setTimeout(function(){
    fields[index].innerHTML = fields[source].innerHTML;
    fields[source].innerHTML = "";
    intoField(source);
    intoMarble(index);
    fillPath("grey"); 
    },2*iteration++)
    setTimeout(function(){
    fillPath("white");
    if(!knockOut(index)) addMarbles();
    active = false;
    },2*iteration+600)
}

function intoField(index){
    fields[index].onclick = function(){fieldClicked(index)};
    fields[index].onmouseover = function(){fieldMouseover(index)};
}

function intoMarble(index){
    fields[index].firstChild.style.top = "0px";
    fields[index].firstChild.style.left = "0px";
    fields[index].onclick = function(){marbleClicked(index)};
    fields[index].onmouseover = function(){if(!active) fillPath("white")};
}

function knockOut(index) {
    var knockedOutPool = [];
    var valuable = false;
    for(var i = 0; i < offsetGroup.length; ++i) {
        var axisPool = []; 
        for(var j = index+offsetGroup[i]; fields[j] != undefined && fields[j].firstChild != null && fields[j].firstChild.style.backgroundColor == fields[index].firstChild.style.backgroundColor && Math.abs(j%width - (j-offsetGroup[i])%width) <= 1; j+=offsetGroup[i]) axisPool.push(j);
        for(var j = index-offsetGroup[i]; fields[j] != undefined && fields[j].firstChild != null && fields[j].firstChild.style.backgroundColor == fields[index].firstChild.style.backgroundColor && Math.abs(j%width - (j+offsetGroup[i])%width) <= 1; j-=offsetGroup[i]) axisPool.push(j);
        if(axisPool.length >= minGroupSize-1) knockedOutPool.push(axisPool);
    }
    if(knockedOutPool.length) knockedOutPool[0].push(index);
    for(var i = 0; i < knockedOutPool.length; ++i) for(var j = 0; j < knockedOutPool[i].length; ++j) fields[knockedOutPool[i][j]].innerHTML = "", intoField(knockedOutPool[i][j]), points.innerHTML = parseInt(points.innerHTML) + 1, --marbleQuantity, valuable = true;
    if(!marbleQuantity) if(confirm("You won!\nWant to play again?")) {init(); return;}
    return valuable;
}

window.onload = init;
